from django.urls import path
from . import views

app_name = "contributor"

urlpatterns = [
    path('<name>/', views.ContributorView.as_view(), name='detail'),
    path('<name>/save_settings/', views.SaveSettings.as_view(), name="save_settings"),
]
