from django.urls import path
from . import views

app_name = "signon"

urlpatterns = [
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('logout/<name>/', views.Logout.as_view(), name='logout_identity'),
    path('oidc/callback/<name>/', views.OIDCAuthenticationCallbackView.as_view(), name='oidc_callback'),
    path('whoami/', views.Whoami.as_view(), name='whoami'),
    path('link/<provider>/<int:pk>/', views.Link.as_view(), name='link'),
    path('unlink/<provider>/', views.Unlink.as_view(), name='unlink'),
]
