from __future__ import annotations
from django.conf import settings
from django.utils.timezone import now
from django.shortcuts import redirect
from django.views.generic import TemplateView, View
from django.contrib import messages
from dc.mixins import VisitorMixin
import contributors.models as cmodels
from contributors.serializers import ContributorSerializer, IdentifierSerializer
from .permissions import IsDD
from rest_framework import viewsets
import requests
import datetime
import os
# from django_filters.rest_framework import DjangoFilterBackend


class Contributors(VisitorMixin, TemplateView):
    template_name = "contributors/contributors.html"

    def get_context_data(self, **kw):
        ctx = super(Contributors, self).get_context_data(**kw)

        current_year = now().year
        year = self.kwargs.get("year", current_year)
        year = int(year)

        ctx["people"] = cmodels.AggregatedPerson.objects.filter(
                until__year=year).order_by("user__full_name").select_related("user")
        ctx["teams"] = cmodels.AggregatedSource.objects.filter(
                until__year=year).order_by("source__name").select_related("source")

        ctx["year"] = year
        ctx["next_year"] = year + 1 if year < current_year else None
        ctx["prev_year"] = year - 1

        return ctx


class ContributorsFlat(VisitorMixin, TemplateView):
    template_name = "contributors/contributors_flat.html"

    def get_context_data(self, **kw):
        ctx = super(ContributorsFlat, self).get_context_data(**kw)
        # Query all people
        ctx["people"] = cmodels.AggregatedPerson.objects.order_by("user__full_name").select_related("user")
        return ctx


class ContributorViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Export per-person aggregated contribution information
    """
    permission_classes = (IsDD,)
    queryset = cmodels.AggregatedPerson.objects.filter(
            user__hidden=False).order_by("user__full_name").select_related("user")
    serializer_class = ContributorSerializer
    # filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user__username', 'user__full_name', 'begin', 'until')


class IdentifierViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Export non-hidden identifier information
    """
    permission_classes = (IsDD,)
    queryset = cmodels.Identifier.objects.filter(
            hidden=False, user__isnull=False, user__hidden=False).order_by("name", "type").select_related("user")
    serializer_class = IdentifierSerializer
    # filter_backends = (DjangoFilterBackend,)
    filter_fields = ('type', 'name', 'user__username', 'user__full_name')


class SourcesFlat(VisitorMixin, TemplateView):
    template_name = "contributors/sources_flat.html"

    def get_context_data(self, **kw):
        ctx = super(SourcesFlat, self).get_context_data(**kw)
        # Query all people
        ctx["teams"] = cmodels.AggregatedSource.objects.order_by("source__name").select_related("source")
        return ctx


class ContributorsNew(VisitorMixin, TemplateView):
    """
    Show contributors, newest first
    """
    template_name = "contributors/contributors_new.html"

    def get_context_data(self, **kw):
        res = super().get_context_data(**kw)
        res["days"] = int(self.request.GET.get("days", 60))
        res["len"] = int(self.request.GET.get("len", 15))
        cutoff = now() - datetime.timedelta(days=res["days"])
        # TODO: this is obsoleted by the move to Salsa
        res["people"] = cmodels.AggregatedPerson.objects\
                               .filter(begin__gte=cutoff)\
                               .filter(user__username__endswith="@alioth")\
                               .order_by("-begin")\
                               .select_related("user")

        res["people"] = [p for p in res["people"] if (p.until - p.begin).days >= res["len"]]
        return res


class ContributorsMIA(VisitorMixin, TemplateView):
    """
    Show Debian Developers who stopped contributing.
    """
    template_name = "contributors/contributors_mia.html"

    def get_context_data(self, **kw):
        res = super(ContributorsMIA, self).get_context_data(**kw)

        # TODO: cache this, using a new "nm" app to talk to nm.debian.org
        # TODO: this also slows down tests considerably, and it makes them require an internet connection
        api_key = getattr(settings, "NM_API_KEY", None)
        if api_key is not None:
            # See https://wiki.debian.org/ServicesSSL#python-requests
            bundle = '/etc/ssl/ca-debian/ca-certificates.crt'
            if os.path.exists(bundle):
                r = requests.get("https://nm.debian.org/api/status?status=dd_u,dd_nu",
                                 headers={"Api-Key": api_key}, verify=bundle)
            else:
                r = requests.get("https://nm.debian.org/api/status?status=dd_u,dd_nu", headers={"Api-Key": api_key})
            people = frozenset(r.json()["people"].keys())
        else:
            people = None

        res["days"] = int(self.request.GET.get("days", 365 * 5))
        cutoff = now() - datetime.timedelta(days=res["days"])
        # TODO: this is obsoleted by Salsa
        res["people"] = cmodels.AggregatedPerson.objects\
                               .filter(until__lte=cutoff)\
                               .filter(user__username__endswith="@debian")\
                               .order_by("-until")\
                               .select_related("user")

        if people is not None:
            res["people"] = [p for p in res["people"] if p.user.email in people]

        return res


class MIAQuery(VisitorMixin, TemplateView):
    require_visitor = "dd"
    template_name = "contributors/mia_query.html"

    def load_objects(self):
        super(MIAQuery, self).load_objects()
        self.query = self.request.GET.get("q")
        if self.query:
            self.ids = list(
                    cmodels.Identifier.objects.filter(name__icontains=self.query, hidden=False)
                                              .select_related("user")
                                              .select_related("user__aggregated_person")
                                              .order_by("name")[:101]
            )
        else:
            self.ids = None

    def get(self, request, *args, **kw):
        if self.ids and len(self.ids) == 1 and self.ids[0].user:
            user = self.ids[0].user
            if request.GET.get("to") == "nm" and user.is_dd:
                return redirect(user.get_nm_url())
            else:
                return redirect(user.get_absolute_url())
        return super(MIAQuery, self).get(request, *args, **kw)

    def get_context_data(self, **kw):
        ctx = super(MIAQuery, self).get_context_data(**kw)
        ctx["q"] = self.query or ""
        if self.query:
            if len(self.ids) > 100:
                ctx["truncated"] = True
            elif not self.ids:
                ctx["empty"] = True
            ctx["ids"] = self.ids
        return ctx


class SiteStatus(VisitorMixin, TemplateView):
    template_name = "contributors/site_status.html"

    def get_context_data(self, **kw):
        ctx = super(SiteStatus, self).get_context_data(**kw)

        ctx["sources"] = cmodels.Source.objects.all().order_by("name")

        from django.db.models import Count
        ctx["stats"] = {
            "unassociated": list(cmodels.Identifier.objects.filter(user=None).values("type")
                                 .annotate(count=Count("type")).order_by("type")),
            "associated": list(cmodels.Identifier.objects.filter(user__isnull=False).values("type")
                               .annotate(count=Count("type")).order_by("type")),
        }

        return ctx


class TestMessages(View):
    def get(self, request, *args, **kw):
        messages.warning(self.request, "Test warning")
        messages.info(self.request, "Test info")
        messages.error(self.request, "Test error")
        # return redirect("contributors")
        return redirect("claim:menu", self.request.user.username)
