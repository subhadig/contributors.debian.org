from django.core.management.base import BaseCommand, CommandError
from contributors.models import User
import getpass
import sys
import logging

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Merge users'

    def add_arguments(self, parser):
        parser.add_argument(
                "--quiet", action="store_true", dest="quiet", default=None,
                help="Disable progress reporting")
        parser.add_argument(
                "--author", action="store", metavar="username", default=getpass.getuser(),
                help="Username of the user performing this operation")
        parser.add_argument(
                "--into", action="store", metavar="dest_username", required=True,
                help="Destination user that other user(s) are merged into")
        parser.add_argument(
                "users", nargs="+", metavar="src_username",
                help="Users to merge into the destination one")

    def handle(self, into, users, author, quiet=False, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if quiet:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        author = User.objects.get(username=author)
        dest = User.objects.get(username=into)
        src = [User.objects.get(username=x) for x in users]

        if dest in src:
            raise CommandError(f"Cannot merge User {src} into itself")

        for user in src:
            dest.merge(user, audit_author=author, audit_notes="merged from command line")
