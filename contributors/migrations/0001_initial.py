# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.CharField(unique=True, max_length=255)),
                ('full_name', models.CharField(max_length=255, blank=True)),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_staff', models.BooleanField(default=False)),
                ('hidden', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AggregatedPerson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('begin', models.DateField()),
                ('until', models.DateField()),
                ('user', models.ForeignKey(related_name='aggregated_sources', to=settings.AUTH_USER_MODEL, unique=True, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AggregatedPersonContribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('begin', models.DateField()),
                ('until', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AggregatedSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('begin', models.DateField()),
                ('until', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('begin', models.DateField()),
                ('until', models.DateField()),
                ('url', models.URLField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContributionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Contribution type name, shown on the website and used as an identifier when submitting contribution data.', max_length=32)),
                ('desc', models.TextField(help_text="Description of this type of contribution. For example: 'wiki editing'", verbose_name='Contribution description')),
                ('contrib_desc', models.TextField(help_text="Description o a person who does contributions of this type. For example: 'wiki editor'.", verbose_name='Contributor description')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Identifier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=16, choices=[('login', 'Debian/Alioth login'), ('fpr', 'OpenPGP key fingerprint'), ('email', 'Email address'), ('wiki', 'Wiki name')])),
                ('name', models.CharField(max_length=256)),
                ('hidden', models.BooleanField(default=False)),
                ('user', models.ForeignKey(related_name='identifiers', to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IdentifierLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ts', models.DateTimeField(auto_now_add=True, verbose_name='timestamp')),
                ('text', models.TextField()),
                ('identifier', models.ForeignKey(related_name='log', to='contributors.Identifier', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Data source name, shown on the website and used as a data source identifier when submitting contribution data.', unique=True, max_length=32)),
                ('desc', models.TextField(help_text='Long description shown on the website at the top of the data source page.', verbose_name='Description')),
                ('url', models.URLField(help_text='Optional URL that links to the home page of the team that provides this data source.', max_length=255, null=True, verbose_name='URL', blank=True)),
                ('auth_token', models.CharField(help_text='Token that is sent alongside contribution information, to avoid random people trolling the site with garbage data.', max_length=255, verbose_name='Authentication token')),
                ('implementation_notes', models.TextField(help_text='A description on how data mining is implemented for this data source, that can be used by people to fix issues if the primary maintainers for it become unavailable.', blank=True)),
                ('last_import', models.DateTimeField(null=True)),
                ('last_contribution', models.DateField(null=True)),
                ('admins', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ts', models.DateTimeField(auto_now_add=True, verbose_name='timestamp')),
                ('text', models.TextField()),
                ('user', models.ForeignKey(related_name='log', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserSourceSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hidden', models.BooleanField(default=False)),
                ('source', models.ForeignKey(related_name='user_settings', to='contributors.Source', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(related_name='source_settings', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='usersourcesettings',
            unique_together=set([('user', 'source')]),
        ),
        migrations.AlterUniqueTogether(
            name='identifier',
            unique_together=set([('type', 'name')]),
        ),
        migrations.AddField(
            model_name='contributiontype',
            name='source',
            field=models.ForeignKey(related_name='contribution_types', to='contributors.Source', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='contributiontype',
            unique_together=set([('source', 'name')]),
        ),
        migrations.AddField(
            model_name='contribution',
            name='identifier',
            field=models.ForeignKey(related_name='contributions', to='contributors.Identifier', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contribution',
            name='type',
            field=models.ForeignKey(related_name='contributions', to='contributors.ContributionType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='contribution',
            unique_together=set([('identifier', 'type')]),
        ),
        migrations.AddField(
            model_name='aggregatedsource',
            name='source',
            field=models.ForeignKey(related_name='aggregated_sources', to='contributors.Source', unique=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aggregatedpersoncontribution',
            name='ctype',
            field=models.ForeignKey(related_name='aggregated_contributions', to='contributors.ContributionType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aggregatedpersoncontribution',
            name='user',
            field=models.ForeignKey(related_name='aggregated_contributions', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='aggregatedpersoncontribution',
            unique_together=set([('user', 'ctype')]),
        ),
    ]
