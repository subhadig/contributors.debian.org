from unittest import TestCase
from sources.utils import list_backup_files, choose_files_to_remove
import datetime
import tempfile
import os


def touch(fname):
    with open(fname, "w+b"):
        pass


class TestBackupCleanup(TestCase):
    def test_list_backup_files(self):
        with tempfile.TemporaryDirectory() as d:
            perl_dir = os.path.join(d, "Perl Packaging")
            os.mkdir(perl_dir)
            touch(os.path.join(perl_dir, "20190929.json.gz"))
            touch(os.path.join(perl_dir, "20190929-test.json.gz"))
            touch(os.path.join(perl_dir, "20190929test.gz"))
            touch(os.path.join(perl_dir, "test.json.gz"))

            files = sorted(list_backup_files(d))

            self.assertEqual(files, [
                os.path.join(d, "Perl Packaging/20190929-test.json.gz"),
                os.path.join(d, "Perl Packaging/20190929.json.gz"),
            ])

    def test_choose_files(self):
        initial = [
            "a b/20190102.json.gz",  # Newer
            "a b/20181231.json.gz",  # 1 day old
            "a b/20181230.json.gz",  # 2 days old
            "a b/20181228.json.gz",  # 4 days old
            "a b/20181226.json.gz",  # 6 days old
            "a b/20181225.json.gz",  # 7 days old
            "a b/20181224.json.gz",  # 8 days old, monday
            "a b/20181223.json.gz",  # 9 days old
            "a b/20181222.json.gz",  # 10 days old
            "a b/20181217.json.gz",  # 15 days old, monday
            "a b/20181216.json.gz",  # 16 days sold
            "a b/20181215.json.gz",  # 17 days old
            "a b/20181210.json.gz",  # 22 days old, monday
            "a b/20181209.json.gz",  # 23 days old
            "a b/20181204.json.gz",  # 28 days old
            "a b/20181203.json.gz",  # 29 days old, monday
            "a b/20181202.json.gz",  # 30 days old
            "a b/20181201.json.gz",  # 31 days old, 1st
            "a b/20181126.json.gz",  # 36 days old, monday
            "a b/20181125.json.gz",  # 37 days old
            "a b/20181120.json.gz",  # 42 days old
            "a b/20181119.json.gz",  # 43 days old, monday
            "a b/20181105.json.gz",  # 57 days old, monday
            "a b/20181103.json.gz",  # 59 days old
            "a b/20181101.json.gz",  # 60 days old, 1st
            "a b/20180806.json.gz",  # 5mo, monday
            "a b/20180801.json.gz",  # 5mo, 1st
            "a b/20180703.json.gz",  # >6mo
            "a b/20180702.json.gz",  # >6mo, monday
            "a b/20180701.json.gz",  # >6mo, 1st
            "a b/20180625.json.gz",  # >6mo, monday
            "a b/20180617.json.gz",  # >6mo
            "a b/20180611.json.gz",  # >6mo, monday
            "a b/20180603.json.gz",  # >6mo
            "a b/20180602.json.gz",  # >6mo
            "a b/20180601.json.gz",  # >6mo, 1st
            "a b/20180101.json.gz",  # 1yo, 1st
            "a b/20170601.json.gz",  # <2yo, 1st
            "a b/20170101.json.gz",  # 2yo, 1st
            "a b/20160601.json.gz",  # >2yo
            "a b/20160101.json.gz",  # >2yo, 1st
            "a b/20151231.json.gz",  # >2yo
            "a b/20150601.json.gz",  # >2yo
            "a b/20150102.json.gz",  # >1to, 1st
            "a b/20150101.json.gz",  # >1to, 1st
        ]

        chosen = list(choose_files_to_remove(initial, now=datetime.date(2019, 1, 1)))

        kept = sorted(set(initial) - set(chosen), reverse=True)

        self.assertEqual(kept, [
            'a b/20190102.json.gz',
            'a b/20181231.json.gz',
            'a b/20181230.json.gz',
            'a b/20181228.json.gz',
            'a b/20181226.json.gz',
            'a b/20181225.json.gz',  # 7 days old
            'a b/20181217.json.gz',  # first after a week
            'a b/20181210.json.gz',  # first after a week
            'a b/20181203.json.gz',  # first after a week
            'a b/20181126.json.gz',  # first after a week
            'a b/20181119.json.gz',  # first after a week
            'a b/20181105.json.gz',  # first after a week
            'a b/20180806.json.gz',  # first after 30 days
            'a b/20180703.json.gz',  # first after 30 days
            'a b/20180603.json.gz',  # first after 30 days
            'a b/20180101.json.gz',  # first after 30 days
            'a b/20170601.json.gz',  # first after 30 days
            'a b/20170101.json.gz',  # first after 30 days
            'a b/20160101.json.gz',  # first after 1y
            'a b/20150101.json.gz',  # first after 1y
        ])

    def test_choose_files1(self):
        initial = [
            "data/contrib-backups/lists.debian.org/20191002.json.gz",
            "data/contrib-backups/lists.debian.org/20191001.json.gz",
            "data/contrib-backups/lists.debian.org/20190610.json",
            "data/contrib-backups/lists.debian.org/20190929.json.info",
            "data/contrib-backups/lists.debian.org/20190930.json.info",
            "data/contrib-backups/lists.debian.org/20190617.json.info",
            "data/contrib-backups/lists.debian.org/20191004.json.gz",
            "data/contrib-backups/lists.debian.org/20190929.json.gz",
            "data/contrib-backups/lists.debian.org/20191003.json.gz",
            "data/contrib-backups/lists.debian.org/20190927.json.info",
            "data/contrib-backups/lists.debian.org/20191004.json.info",
            "data/contrib-backups/lists.debian.org/20190610.json.info",
            "data/contrib-backups/lists.debian.org/20190624.json",
            "data/contrib-backups/lists.debian.org/20191002.json.info",
            "data/contrib-backups/lists.debian.org/20190601.json",
            "data/contrib-backups/lists.debian.org/20190617.json",
            "data/contrib-backups/lists.debian.org/20190928.json.gz",
            "data/contrib-backups/lists.debian.org/20190928.json.info",
            "data/contrib-backups/lists.debian.org/20191003.json.info",
            "data/contrib-backups/lists.debian.org/20190603.json.info",
            "data/contrib-backups/lists.debian.org/20190601.json.info",
            "data/contrib-backups/lists.debian.org/20190603.json",
            "data/contrib-backups/lists.debian.org/20190927.json.gz",
            "data/contrib-backups/lists.debian.org/20190930.json.gz",
            "data/contrib-backups/lists.debian.org/20191001.json.info",
            "data/contrib-backups/lists.debian.org/20190624.json.info",
            "data/contrib-backups/foo.debian.org/20191002.json.gz",
            "data/contrib-backups/foo.debian.org/20191001.json.gz",
            "data/contrib-backups/foo.debian.org/20190610.json",
            "data/contrib-backups/foo.debian.org/20190929.json.info",
            "data/contrib-backups/foo.debian.org/20190930.json.info",
            "data/contrib-backups/foo.debian.org/20190617.json.info",
            "data/contrib-backups/foo.debian.org/20191004.json.gz",
            "data/contrib-backups/foo.debian.org/20190929.json.gz",
            "data/contrib-backups/foo.debian.org/20191003.json.gz",
            "data/contrib-backups/foo.debian.org/20190927.json.info",
            "data/contrib-backups/foo.debian.org/20191004.json.info",
            "data/contrib-backups/foo.debian.org/20190610.json.info",
            "data/contrib-backups/foo.debian.org/20190624.json",
            "data/contrib-backups/foo.debian.org/20191002.json.info",
            "data/contrib-backups/foo.debian.org/20190601.json",
            "data/contrib-backups/foo.debian.org/20190617.json",
            "data/contrib-backups/foo.debian.org/20190928.json.gz",
            "data/contrib-backups/foo.debian.org/20190928.json.info",
            "data/contrib-backups/foo.debian.org/20191003.json.info",
            "data/contrib-backups/foo.debian.org/20190603.json.info",
            "data/contrib-backups/foo.debian.org/20190601.json.info",
            "data/contrib-backups/foo.debian.org/20190603.json",
            "data/contrib-backups/foo.debian.org/20190927.json.gz",
            "data/contrib-backups/foo.debian.org/20190930.json.gz",
            "data/contrib-backups/foo.debian.org/20191001.json.info",
            "data/contrib-backups/foo.debian.org/20190624.json.info",
        ]

        chosen = list(choose_files_to_remove(initial, now=datetime.date(2019, 10, 4)))

        kept = sorted(set(initial) - set(chosen), reverse=True)

        self.maxDiff = None
        self.assertEqual(kept, [
            'data/contrib-backups/lists.debian.org/20191004.json.info',
            'data/contrib-backups/lists.debian.org/20191004.json.gz',
            'data/contrib-backups/lists.debian.org/20191003.json.info',
            'data/contrib-backups/lists.debian.org/20191003.json.gz',
            'data/contrib-backups/lists.debian.org/20191002.json.info',
            'data/contrib-backups/lists.debian.org/20191002.json.gz',
            'data/contrib-backups/lists.debian.org/20191001.json.info',
            'data/contrib-backups/lists.debian.org/20191001.json.gz',
            'data/contrib-backups/lists.debian.org/20190930.json.info',
            'data/contrib-backups/lists.debian.org/20190930.json.gz',
            'data/contrib-backups/lists.debian.org/20190929.json.info',
            'data/contrib-backups/lists.debian.org/20190929.json.gz',
            'data/contrib-backups/lists.debian.org/20190928.json.info',
            'data/contrib-backups/lists.debian.org/20190928.json.gz',
            'data/contrib-backups/lists.debian.org/20190927.json.info',
            'data/contrib-backups/lists.debian.org/20190927.json.gz',
            'data/contrib-backups/lists.debian.org/20190624.json.info',
            'data/contrib-backups/lists.debian.org/20190624.json',
            'data/contrib-backups/lists.debian.org/20190617.json.info',
            'data/contrib-backups/lists.debian.org/20190617.json',
            'data/contrib-backups/lists.debian.org/20190610.json.info',
            'data/contrib-backups/lists.debian.org/20190610.json',
            'data/contrib-backups/lists.debian.org/20190603.json.info',
            'data/contrib-backups/lists.debian.org/20190603.json',

            'data/contrib-backups/foo.debian.org/20191004.json.info',
            'data/contrib-backups/foo.debian.org/20191004.json.gz',
            'data/contrib-backups/foo.debian.org/20191003.json.info',
            'data/contrib-backups/foo.debian.org/20191003.json.gz',
            'data/contrib-backups/foo.debian.org/20191002.json.info',
            'data/contrib-backups/foo.debian.org/20191002.json.gz',
            'data/contrib-backups/foo.debian.org/20191001.json.info',
            'data/contrib-backups/foo.debian.org/20191001.json.gz',
            'data/contrib-backups/foo.debian.org/20190930.json.info',
            'data/contrib-backups/foo.debian.org/20190930.json.gz',
            'data/contrib-backups/foo.debian.org/20190929.json.info',
            'data/contrib-backups/foo.debian.org/20190929.json.gz',
            'data/contrib-backups/foo.debian.org/20190928.json.info',
            'data/contrib-backups/foo.debian.org/20190928.json.gz',
            'data/contrib-backups/foo.debian.org/20190927.json.info',
            'data/contrib-backups/foo.debian.org/20190927.json.gz',
            'data/contrib-backups/foo.debian.org/20190624.json.info',
            'data/contrib-backups/foo.debian.org/20190624.json',
            'data/contrib-backups/foo.debian.org/20190617.json.info',
            'data/contrib-backups/foo.debian.org/20190617.json',
            'data/contrib-backups/foo.debian.org/20190610.json.info',
            'data/contrib-backups/foo.debian.org/20190610.json',
            'data/contrib-backups/foo.debian.org/20190603.json.info',
            'data/contrib-backups/foo.debian.org/20190603.json',
        ])
