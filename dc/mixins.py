from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from dclayout.mixins import DCLayoutMixin


class VisitorMixin(DCLayoutMixin):
    """
    Base infrastructure for plugging in permission checking and loading of common objects
    """
    # Define as "user", "dd", or "admin" to raise PermissionDenied if the given
    # test on the visitor fails
    require_visitor = None

    def load_objects(self):
        """
        Hook to set self.* members from request parameters, so that they are
        available to the rest of the view members.
        """
        pass

    def check_visitor_permission(self, perm):
        """
        Check the visitor against self.require_visitor
        """
        if self.require_visitor == "user":
            return True
        elif self.require_visitor == "dd":
            return self.request.user.is_authenticated and self.request.user.is_dd
        elif self.require_visitor == "admin":
            return self.request.user.is_superuser
        else:
            return False

    def check_permissions(self):
        """
        Raise PermissionDenied if some of the permissions requested by the view
        configuration are not met.

        Subclasses can extend this to check their own permissions.
        """
        if self.request.user.is_superuser:
            return

        if self.require_visitor:
            if not self.request.user.is_authenticated:
                raise PermissionDenied
            if isinstance(self.require_visitor, str):
                if not self.check_visitor_permission(self.require_visitor):
                    raise PermissionDenied
            else:
                if not any(self.check_visitor_permission(x) for x in self.require_visitor):
                    raise PermissionDenied

    def pre_dispatch(self):
        pass

    def dispatch(self, request, *args, **kwargs):
        response = self.load_objects()
        # Useful when load_objects() redirects to another url
        if response is not None and isinstance(response, HttpResponse):
            return response

        self.check_permissions()
        self.pre_dispatch()
        return super().dispatch(request, *args, **kwargs)
