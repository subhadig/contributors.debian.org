from __future__ import annotations
from django.urls import include, path
from django.views.generic import TemplateView
import django.conf.urls
from contributors import views as cviews
import dclayout.views as dcviews
from rest_framework import routers

from django.contrib import admin
admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'contributors', cviews.ContributorViewSet, "contributors")
router.register(r'identifiers', cviews.IdentifierViewSet, "identifiers")

django.conf.urls.handler403 = dcviews.Error403.as_view()
django.conf.urls.handler404 = dcviews.Error404.as_view()
django.conf.urls.handler500 = dcviews.Error500.as_view()


urlpatterns = [
    path('', cviews.Contributors.as_view(), name="contributors"),
    path('license/', TemplateView.as_view(template_name='license.html'), name="root_license"),
    path('about/privacy/', TemplateView.as_view(template_name='privacy.html'), name="root_privacy"),
    path('admin/', admin.site.urls),
    path('contributors/', include('contributors.urls')),
    path('contributor/', include('contributor.urls')),
    path('source/', include("sources.urls")),
    path('mia/', include("mia.urls")),
    path('importer/', include("importer.urls")),
    path('claim/', include("claim.urls")),
    path('api/', include(router.urls)),
    path('deploy/', include("deploy.urls")),
    path('signon/', include("signon.urls")),
    path('impersonate/', include("impersonate.urls")),
]
