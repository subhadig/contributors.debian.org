from __future__ import annotations
from urllib.parse import urlparse
from django.test import TestCase
from django.core import mail, signing
from dc.unittest import SourceFixtureMixin
from django.urls import reverse


class ClaimFixtureMixin(SourceFixtureMixin):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.idents.create("new_email", type="email", name="new@example.org")
        cls.idents.create("new_login", type="login", name="new")
        cls.idents.create("new_fpr", type="fpr", name="0123456789ABCDEF0123456789ABCDEF012345678")


class TestClaimWelcome(ClaimFixtureMixin, TestCase):
    def test_claim_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:claim"))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:claim"), data={"username": self.users.dd.username})
        self.assertPermissionDenied(response)

    def test_claim_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:claim"))
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

        response = client.post(reverse("claim:claim"), data={"username": self.users.dd.username})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

    def test_claim_start(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:claim"))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:claim"), data={"username": self.users.dd.username})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd.username]))

    def test_claim_other_user(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:claim"))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:claim"), data={"username": self.users.dd1.username})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd1.username]))

    def test_claim_invalid_user(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:claim"))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:claim"), data={"username": "invalid"})
        self.assertEqual(response.status_code, 200)


class TestClaimMenu(ClaimFixtureMixin, TestCase):
    def test_claim_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:menu", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:menu", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

    def test_claim_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:menu", args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:menu", args=[self.users.alioth1.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:menu", args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 405)

    def test_claim_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:menu", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:menu", args=[self.users.dd1.username]))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:menu", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 405)


class TestClaimEmail(ClaimFixtureMixin, TestCase):
    def test_claim_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:email", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:email", args=[self.users.alioth.username]),
                               data={"email": self.idents.new_email.name})
        self.assertPermissionDenied(response)

    def test_claim_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:email", args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("emails", response.context)

        response = client.get(reverse("claim:email", args=[self.users.alioth1.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:email", args=[self.users.alioth.username]), data={"email": "n"})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("emails", response.context)

        response = client.post(reverse("claim:email", args=[self.users.alioth.username]),
                               data={"email": self.idents.new_email.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

        # Claim did not happen
        self.idents.new_email.refresh_from_db()
        self.assertIsNone(self.idents.new_email.user)

        # A challenge email has been sent
        self.assertEqual(len(mail.outbox), 1)
        m = mail.outbox[0]
        lines = m.body.splitlines()
        self.assertEqual(lines[:8], [
            'Hello,',
            '',
            'alioth-guest@alioth (who is probably you) wants to be credited on '
            'https://contributors.debian.org',
            'for contributions made with this email address (new@example.org).',
            '',
            'If that is NOT correct, please ignore or delete this email.',
            '',
            'If that is correct, please confirm by clicking this link, while you are logged in:',
        ])
        self.assertEqual(lines[9:], [
            '',
            'Thank you,',
            '',
            'contributors.debian.org',
        ])

        u = urlparse(lines[8])
        self.assertEqual(u.path, reverse("claim:verify_email", args=[self.users.alioth]))
        self.assertRegex(u.query, r"^token=[^&]+$")
        encoded = u.query[6:]
        token = signing.loads(encoded)
        self.assertEqual(token, {'u': self.users.alioth.pk, 'i': self.idents.new_email.pk})

        # Visit the confirmation link
        response = client.get(lines[8])
        self.assertEqual(response.status_code, 200)
        self.idents.new_email.refresh_from_db()
        self.assertIsNone(self.idents.new_email.user)

        # Post to the confirmation link
        response = client.post(reverse("claim:verify_email", args=[self.users.alioth]), data={"token": encoded})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

        # Claim happened
        self.idents.new_email.refresh_from_db()
        self.assertEqual(self.idents.new_email.user, self.users.alioth)

    def test_claim_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:email", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:email", args=[self.users.dd1.username]))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:email", args=[self.users.dd.username]),
                               data={"email": self.idents.new_email.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd.username]))
        self.idents.new_email.refresh_from_db()
        self.assertEqual(self.idents.new_email.user, self.users.dd)

        response = client.post(reverse("claim:email", args=[self.users.alioth.username]), data={"email": "n"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("emails", response.context)

        response = client.post(reverse("claim:email", args=[self.users.dd1.username]),
                               data={"email": self.idents.new_email.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd1.username]))
        self.idents.new_email.refresh_from_db()
        self.assertEqual(self.idents.new_email.user, self.users.dd1)


class TestClaimFpr(ClaimFixtureMixin, TestCase):
    def test_claim_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:fpr", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:fpr", args=[self.users.alioth.username]),
                               data={"fpr": self.idents.new_fpr.name})
        self.assertPermissionDenied(response)

    def test_claim_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:fpr", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.get(reverse("claim:fpr", args=[self.users.alioth1.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:fpr", args=[self.users.alioth.username]),
                               data={"fpr": self.idents.new_fpr.name})
        self.assertPermissionDenied(response)

    def test_claim_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:fpr", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:fpr", args=[self.users.dd1.username]))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:fpr", args=[self.users.dd.username]),
                               data={"fpr": self.idents.new_fpr.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd.username]))
        self.idents.new_fpr.refresh_from_db()
        self.assertEqual(self.idents.new_fpr.user, self.users.dd)

        response = client.post(reverse("claim:fpr", args=[self.users.dd1.username]),
                               data={"fpr": self.idents.new_fpr.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd1.username]))
        self.idents.new_fpr.refresh_from_db()
        self.assertEqual(self.idents.new_fpr.user, self.users.dd1)


class TestClaimLogin(ClaimFixtureMixin, TestCase):
    def test_claim_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:login", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:login", args=[self.users.alioth.username]),
                               data={"login": self.idents.new_login.name})
        self.assertPermissionDenied(response)

    def test_claim_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:login", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.get(reverse("claim:login", args=[self.users.alioth1.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:login", args=[self.users.alioth.username]),
                               data={"login": self.idents.new_login.name})
        self.assertPermissionDenied(response)

    def test_claim_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:login", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:login", args=[self.users.dd1.username]))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:login", args=[self.users.dd.username]),
                               data={"login": self.idents.new_login.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd.username]))
        self.idents.new_login.refresh_from_db()
        self.assertEqual(self.idents.new_login.user, self.users.dd)

        response = client.post(reverse("claim:login", args=[self.users.dd1.username]),
                               data={"login": self.idents.new_login.name})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.dd1.username]))
        self.idents.new_login.refresh_from_db()
        self.assertEqual(self.idents.new_login.user, self.users.dd1)


class TestEmailVerify(ClaimFixtureMixin, TestCase):
    def test_correct_unassigned(self):
        token = signing.dumps({'u': self.users.alioth.pk, 'i': self.idents.new_email.pk})

        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse('claim:verify_email', args=[self.users.alioth.username]) + f"?token={token}")
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse('claim:verify_email', args=[self.users.alioth.username]), data={"token": token})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

        self.idents.new_email.refresh_from_db()
        self.assertEqual(self.idents.new_email.user, self.users.alioth)

    def test_correct_assigned(self):
        self.idents.new_login.user = self.users.dd
        self.idents.new_login.save()

        token = signing.dumps({'u': self.users.alioth.pk, 'i': self.idents.new_email.pk})

        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse('claim:verify_email', args=[self.users.alioth.username]) + f"?token={token}")
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse('claim:verify_email', args=[self.users.alioth.username]), data={"token": token})
        self.assertRedirects(response, reverse("claim:menu", args=[self.users.alioth.username]))

        self.idents.new_email.refresh_from_db()
        self.assertEqual(self.idents.new_email.user, self.users.alioth)

    def test_correct_anonymous(self):
        token = signing.dumps({'u': self.users.alioth.pk, 'i': self.idents.new_email.pk})

        client = self.make_test_client(None)
        response = client.get(reverse('claim:verify_email', args=[self.users.alioth.username]) + f"?token={token}")
        self.assertPermissionDenied(response)

        self.idents.new_email.refresh_from_db()
        self.assertIsNone(self.idents.new_email.user)

    def test_no_token(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse('claim:verify_email', args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 404)

        self.idents.new_email.refresh_from_db()
        self.assertIsNone(self.idents.new_email.user)

    def test_invalid_token(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse('claim:verify_email', args=[self.users.alioth.username]) + f"?token=invalid")
        self.assertEqual(response.status_code, 404)

        self.idents.new_email.refresh_from_db()
        self.assertIsNone(self.idents.new_email.user)


class TestUnclaimMenu(ClaimFixtureMixin, TestCase):
    def test_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:unclaim_menu", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:unclaim_menu", args=[self.users.alioth.username]))
        self.assertPermissionDenied(response)

    def test_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:unclaim_menu", args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:unclaim_menu", args=[self.users.alioth1.username]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:unclaim_menu", args=[self.users.alioth.username]))
        self.assertEqual(response.status_code, 405)

    def test_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:unclaim_menu", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:unclaim_menu", args=[self.users.dd1.username]))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse("claim:unclaim_menu", args=[self.users.dd.username]))
        self.assertEqual(response.status_code, 405)


class TestUnclaim(ClaimFixtureMixin, TestCase):
    def test_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("claim:unclaim", args=[self.users.alioth.username, self.idents.alioth_user.pk]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:unclaim", args=[self.users.alioth.username, self.idents.alioth_user.pk]))
        self.assertPermissionDenied(response)

    def test_nondd(self):
        client = self.make_test_client(self.users.alioth)
        response = client.get(reverse("claim:unclaim", args=[self.users.alioth.username, self.idents.alioth_user.pk]))
        self.assertEqual(response.status_code, 200)

        self.idents.alioth_user.refresh_from_db()
        self.assertEqual(self.idents.alioth_user.user, self.users.alioth)

        response = client.get(reverse("claim:unclaim", args=[self.users.alioth1.username, self.idents.alioth1_user.pk]))
        self.assertPermissionDenied(response)

        response = client.get(reverse("claim:unclaim", args=[self.users.alioth.username, self.idents.alioth1_user.pk]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:unclaim", args=[self.users.alioth.username, self.idents.alioth_user.pk]))
        self.assertRedirects(response, reverse("claim:unclaim_menu", args=[self.users.alioth.username]))

        self.idents.alioth_user.refresh_from_db()
        self.assertIsNone(self.idents.alioth_user.user)

    def test_dd(self):
        client = self.make_test_client(self.users.dd)
        response = client.get(reverse("claim:unclaim", args=[self.users.dd.username, self.idents.dd_user.pk]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:unclaim", args=[self.users.dd1.username, self.idents.dd1_user.pk]))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("claim:unclaim", args=[self.users.dd.username, self.idents.dd1_user.pk]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("claim:unclaim", args=[self.users.dd.username, self.idents.dd_user.pk]))
        self.assertRedirects(response, reverse("claim:unclaim_menu", args=[self.users.dd.username]))
        self.idents.dd_user.refresh_from_db()
        self.assertIsNone(self.idents.dd_user.user)

        response = client.post(reverse("claim:unclaim", args=[self.users.dd1.username, self.idents.dd1_user.pk]))
        self.assertRedirects(response, reverse("claim:unclaim_menu", args=[self.users.dd1.username]))
        self.idents.dd1_user.refresh_from_db()
        self.assertIsNone(self.idents.dd1_user.user)

        response = client.post(reverse("claim:unclaim", args=[self.users.dd.username, self.idents.dd1_home.pk]))
        self.assertPermissionDenied(response)
        self.idents.dd1_home.refresh_from_db()
        self.assertEqual(self.idents.dd1_home.user, self.users.dd1)
