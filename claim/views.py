from __future__ import annotations
from django.core.exceptions import PermissionDenied
from django.core import mail, signing
from django import http
from django.conf import settings
from django.urls import reverse
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from dc.mixins import VisitorMixin
import contributors.models as cmodels
from .forms import UsernameForm, EmailIdentityForm, FingerprintIdentityForm, LoginIdentityForm


class Claim(VisitorMixin, FormView):
    template_name = "claim/claim.html"
    form_class = UsernameForm
    require_visitor = "user"

    def get(self, request, *args, **kw):
        if not self.request.user.is_dd:
            return redirect("claim:menu", self.request.user.username)
        else:
            return super().get(request, *args, **kw)

    def post(self, request, *args, **kw):
        dest = self.request.POST.get("action", "claim")
        if dest == "claim":
            if not self.request.user.is_dd:
                return redirect("claim:menu", self.request.user.username)
            else:
                return super().post(request, *args, **kw)
        else:
            if not self.request.user.is_dd:
                return redirect("claim:unclaim_menu", self.request.user.username)
            else:
                return super().post(request, *args, **kw)

    def get_initial(self):
        return {"username": self.request.user.username}

    def form_valid(self, form):
        dest = self.request.POST.get("action", "claim")
        if dest == "claim":
            return redirect("claim:menu", form.cleaned_data["username"])
        else:
            return redirect("claim:unclaim_menu", form.cleaned_data["username"])


class ClaimUserMixin:
    def load_objects(self):
        super().load_objects()
        self.target = get_object_or_404(cmodels.User, username=self.kwargs["username"])

    def check_permissions(self):
        super().check_permissions()
        if not self.request.user.is_dd and self.target != self.request.user:
            raise PermissionDenied()

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["target"] = self.target
        return ctx


class ClaimUser(ClaimUserMixin, VisitorMixin, TemplateView):
    template_name = "claim/menu.html"
    require_visitor = "user"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["login_form"] = LoginIdentityForm()
        ctx["fpr_form"] = FingerprintIdentityForm()
        ctx["email_form"] = EmailIdentityForm()
        return ctx


class ClaimEmail(ClaimUserMixin, VisitorMixin, FormView):
    template_name = "claim/claim_user_email.html"
    require_visitor = "user"
    form_class = EmailIdentityForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        email = self.request.POST.get("email")
        if self.request.user.is_dd and email is not None:
            if "@" not in email:
                email += "@"
            ctx["emails"] = cmodels.Identifier.objects.filter(type="email", name__istartswith=email).order_by("name")
        return ctx

    def form_valid(self, form):
        ident = get_object_or_404(cmodels.Identifier, type="email", name=form.cleaned_data["email"])

        if self.request.user.is_dd:
            # Do the association directly
            msg = f"Email {ident.name} associated to {self.target} from {ident.user} by {self.request.user}"
            ident.user = self.target
            ident.save()
            ident.add_log(msg)
        else:
            # Send email challenge
            token = signing.dumps({'u': self.request.user.pk, 'i': ident.pk})
            verify_url = reverse('claim:verify_email', args=[self.target.username])
            url = self.request.build_absolute_uri(f"{verify_url}?token={token}")
            message = f"""Hello,

{self.request.user} (who is probably you) wants to be credited on https://contributors.debian.org
for contributions made with this email address ({ident.name}).

If that is NOT correct, please ignore or delete this email.

If that is correct, please confirm by clicking this link, while you are logged in:
{url}

Thank you,

contributors.debian.org
"""
            mail_from = '{0} <{1}>'.format(settings.ADMINS[0][0], settings.ADMINS[0][1])
            mail.send_mail('Verify contributors.debian.org claim', message,
                           mail_from, [ident.name], fail_silently=False)
            msg = f"Challenge email sent to {ident.name}. Click on the verification link to complete the process."

        messages.info(self.request, msg)
        return redirect("claim:menu", self.target.username)


class ClaimLogin(ClaimUserMixin, VisitorMixin, FormView):
    template_name = "claim/claim_user_login.html"
    require_visitor = "dd"
    form_class = LoginIdentityForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        login = self.request.POST.get("login")
        if login is not None:
            ctx["logins"] = cmodels.Identifier.objects.filter(type="login", name__icontains=login).order_by("name")
        return ctx

    def form_valid(self, form):
        ident = get_object_or_404(cmodels.Identifier, type="login", name=form.cleaned_data["login"])
        msg = f"Login {ident.name} associated to {self.target} from {ident.user} by {self.request.user}"
        ident.user = self.target
        ident.save()
        ident.add_log(msg)
        messages.info(self.request, msg)
        return redirect("claim:menu", self.target.username)


class ClaimFingerprint(ClaimUserMixin, VisitorMixin, FormView):
    template_name = "claim/claim_user_fpr.html"
    require_visitor = "dd"
    form_class = FingerprintIdentityForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        fpr = self.request.POST.get("fpr")
        if fpr is not None:
            ctx["fprs"] = cmodels.Identifier.objects.filter(type="fpr", name__icontains=fpr).order_by("name")
        return ctx

    def form_valid(self, form):
        ident = get_object_or_404(cmodels.Identifier, type="fpr", name=form.cleaned_data["fpr"])
        msg = f"Fingreprint {ident.name} associated to {self.target} from {ident.user} by {self.request.user}"
        ident.user = self.target
        ident.save()
        ident.add_log(msg)
        messages.info(self.request, msg)
        return redirect("claim:menu", self.target.username)


class ClaimEmailVerify(ClaimUserMixin, VisitorMixin, TemplateView):
    template_name = "claim/validate_email.html"
    require_visitor = "user"

    def load_objects(self):
        super().load_objects()
        if self.request.method == "GET":
            self.token = self.request.GET.get("token")
        else:
            self.token = self.request.POST.get("token")
        if self.token is None:
            raise http.Http404

        try:
            self.decoded = signing.loads(self.token, max_age=3600 * 24 * 3)
        except (signing.BadSignature, signing.SignatureExpired):
            raise http.Http404

        self.ident = get_object_or_404(cmodels.Identifier, pk=self.decoded["i"])

    def check_permissions(self):
        super().check_permissions()
        user = get_object_or_404(cmodels.User, pk=self.decoded["u"])
        if user != self.target:
            raise PermissionDenied()

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["token"] = self.token
        ctx["ident"] = self.ident
        return ctx

    def post(self, request, *args, **kw):
        # Do the association
        msg = f"Email {self.ident.name} associated to {self.target} from {self.ident.user} by {self.request.user}"
        self.ident.user = self.target
        self.ident.save()
        self.ident.add_log(msg)
        messages.info(self.request, msg)
        return redirect("claim:menu", self.target.username)


class UnclaimMenu(ClaimUserMixin, VisitorMixin, TemplateView):
    template_name = "claim/unclaim_menu.html"
    require_visitor = "user"


class Unclaim(ClaimUserMixin, VisitorMixin, TemplateView):
    template_name = "claim/unclaim.html"
    require_visitor = "user"

    def load_objects(self):
        super().load_objects()
        self.ident = get_object_or_404(cmodels.Identifier, pk=self.kwargs["ident"])

    def check_permissions(self):
        super().check_permissions()
        if self.ident.user != self.target:
            raise PermissionDenied()

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["ident"] = self.ident
        return ctx

    def post(self, request, *args, **kw):
        msg = f"{self.ident.type} {self.ident.name} deassociated from {self.target} by {self.request.user}"
        self.ident.user = None
        self.ident.save()
        self.ident.add_log(msg)
        self.target.add_log(msg)
        messages.info(self.request, msg)
        return redirect("claim:unclaim_menu", self.target.username)
