from __future__ import annotations
from django.urls import path
from . import views

app_name = "claim"

urlpatterns = [
    path('', views.Claim.as_view(), name="claim"),
    path('<username>/', views.ClaimUser.as_view(), name="menu"),
    path('<username>/email/', views.ClaimEmail.as_view(), name="email"),
    path('<username>/login/', views.ClaimLogin.as_view(), name="login"),
    path('<username>/fpr/', views.ClaimFingerprint.as_view(), name="fpr"),
    path('<username>/email/verify', views.ClaimEmailVerify.as_view(), name="verify_email"),
    path('<username>/unclaim/', views.UnclaimMenu.as_view(), name="unclaim_menu"),
    path('<username>/unclaim/<int:ident>/', views.Unclaim.as_view(), name="unclaim"),
    # path('<name>/unclaim/', views.Unclaim.as_view(), name="unclaim"),
]
