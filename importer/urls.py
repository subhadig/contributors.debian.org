from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^logs$', views.Logs.as_view(), name="importer_logs"),
    url(r'^status$', views.Status.as_view(), name="importer_status"),
    url(r'^status/(?P<sname>[^/]+)$', views.SourceStatus.as_view(), name="importer_source_status"),
    url(r'^submission/(?P<pk>\d+)$', views.ShowSubmission.as_view(), name="importer_submission"),
]
