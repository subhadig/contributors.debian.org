from django.core.management.base import BaseCommand
from django.conf import settings
from importer.importer import Importer
from importer.models import Submission
from contextlib import contextmanager
import logging
import os
import fcntl

log = logging.getLogger(__name__)


@contextmanager
def lock():
    lockfile = os.path.join(settings.DATA_DIR, "trigger", "import_lock")
    with open(lockfile, "wb") as fd:
        fcntl.lockf(fd.fileno(), fcntl.LOCK_EX, 0, 1)
        try:
            yield
        finally:
            fcntl.lockf(fd.fileno(), fcntl.LOCK_UN, 0, 1)


class Command(BaseCommand):
    help = 'Import pending submissions'

    def add_arguments(self, parser):
        parser.add_argument("--list", action="store_true", help="list pending submissions only")
        parser.add_argument("--test-lock", action="store_true", help="test locking the lockfile")

    def handle(self, *args, **opts):
        if opts["list"]:
            for submission in Submission.objects.pending().order_by("source", "received"):
                print("{s.source.name}:{s.received:%Y-%m-%d %H:%M}: {s.method}".format(s=submission))
        elif opts["test_lock"]:
            with lock():
                input("Press enter to unlock> ")
        else:
            with lock():
                for submission in Submission.objects.pending().order_by("source", "received"):
                    imp = Importer()
                    try:
                        imp.import_submission(submission)
                    except Exception:
                        log.exception(
                                "%s:%s:%s: failed to import",
                                submission.source.name, submission.received, submission.method)
                Importer.clear_pending_trigger_file()
