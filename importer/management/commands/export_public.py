from django.core.management.base import BaseCommand
from importer.exporter import export_public_data
import sys
import logging
import json

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Export data source information'

    def add_arguments(self, parser):
        parser.add_argument(
                "--quiet", action="store_true", dest="quiet", default=None,
                help="Disable progress reporting")

    def handle(self, quiet=False, with_tokens=False, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if quiet:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        data = export_public_data()
        json.dump(data, sys.stdout)
