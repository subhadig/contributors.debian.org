from __future__ import annotations
from django.utils.translation import ugettext as _
from django.views.generic import View
from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.contrib.auth import get_user_model
from django import http


class Impersonate(View):
    def post(self, request, *args, **kw):
        User = get_user_model()
        effective_user = getattr(request, "impersonator", None)
        if effective_user is None:
            effective_user = request.user
        if not effective_user.is_authenticated or not effective_user.is_staff:
            raise PermissionDenied
        pk = request.POST.get("pk")
        if pk is None:
            del request.session["impersonate"]
            messages.add_message(request, messages.INFO, _("Impersonation canceled"))
            user = effective_user
        else:
            try:
                user = User.objects.get(pk=pk)
            except User.DoesNotExist:
                raise PermissionDenied
            request.session["impersonate"] = user.pk
            messages.info(request, _("Impersonating {}").format(user))

        url = request.POST.get("next", None)
        if url is None:
            return redirect(user.get_absolute_url())
        else:
            return redirect(url)


class Whoami(View):
    def get(self, request, *args, **kw):
        impersonator = getattr(request, "impersonator", None)
        return http.JsonResponse({
            "user": request.user.pk,
            "user_desc": str(request.user),
            "impersonator": None if impersonator is None else impersonator.pk,
            "impersonator_desc": None if impersonator is None else str(impersonator),
        })
